# banco-angular-ionic-firebase

Aplicación demo de banco con ionic, firebase y cordova

## Instalacion

Instalar IONIC CLI

```
npm install -g @ionic/cli
```

Correr el proyecto (Solo web)

```
ionic serve
```


para agregar la plataforma para android

```
ionic cordova platform add android
```
para construir el apk para android, se debe  correr el siguiente comando

```
ionic cordova build android
```

## Nota

En la raiz del proyecto hay un apk ya generado.

## Login

usuario: usuario@banco.com
password: Qwerty123

## QR

![QR](qr.jpeg)
