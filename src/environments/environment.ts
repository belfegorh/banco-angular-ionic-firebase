// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyDooeCe7JEk3QS8cy-N48pXiQ4LPgJdOjo",
    authDomain: "banco-angular-ionic-firebase.firebaseapp.com",
    projectId: "banco-angular-ionic-firebase",
    storageBucket: "banco-angular-ionic-firebase.appspot.com",
    messagingSenderId: "327329252493",
    appId: "1:327329252493:web:5063b416e94740388fcec4",
    measurementId: "G-QXT93421QC"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
