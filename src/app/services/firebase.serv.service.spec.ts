import { TestBed } from '@angular/core/testing';

import { Firebase.ServService } from './firebase.serv.service';

describe('Firebase.ServService', () => {
  let service: Firebase.ServService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(Firebase.ServService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
