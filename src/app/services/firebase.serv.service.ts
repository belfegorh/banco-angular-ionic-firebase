import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { Router } from "@angular/router";
import { User } from '@angular/fire/auth';
import { IndicatorsService } from "./indicators.service";


@Injectable({
  providedIn: 'root'
})
export class FirebaseService {

  user: User;

  constructor(public afAuth: AngularFireAuth, public router: Router,    public indicator:IndicatorsService
    ) {
    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.user = user;
        localStorage.setItem('user', JSON.stringify(this.user));
      } else {
        localStorage.setItem('user', null);
      }
    })
  }
  async login(email: string, password: string) {
    var result = await this.afAuth.signInWithEmailAndPassword(email, password).then(async res=>{
      const toast:any = await this.indicator.presentToast("success","Bienvenido")
      toast.present();
    }).catch(async err=>{
      console.log(err)
      const toast:any = await this.indicator.presentToast("danger","Usuario y/o password incorrectos")
      toast.present();
    })
    this.router.navigate(['/home']);
  }
  async logout() {
    await this.afAuth.signOut();
    localStorage.removeItem('user');
    this.router.navigate(['/login']);
  }

  get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('user'));
    return user !== null;
  }
  public getUser(): User {
    return this.user
  }

}
