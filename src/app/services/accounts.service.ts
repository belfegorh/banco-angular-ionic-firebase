import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Observable, Subscription } from 'rxjs';
import { trace } from '@angular/fire/compat/performance';

@Injectable({
  providedIn: 'root'
})
export class AccountsService {
  user = JSON.parse(localStorage.getItem('user'))
  ref = 'cuentas/' + this.user.uid

  constructor(    
    public firestore: AngularFirestore,

    ) { 
      
    }
  async getAccounts(){
    const response = await this.firestore.doc(this.ref).get().toPromise();
    
    return response
  }
  async updateAccounts(doc){
    const response = await this.firestore.doc(this.ref).set(doc,{merge: true});
    
    return response
  }
}
