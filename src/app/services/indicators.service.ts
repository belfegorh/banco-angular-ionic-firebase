import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class IndicatorsService {

  constructor(
    public loadingController: LoadingController,
    public toastController:ToastController

  ) { }
  async generateLoader() {
    const loading = await this.loadingController.create({
      message: 'Please wait...'
    });
    // await loading.present();
    return loading

  }
  async presentToast(color,message) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
      color:color
    });
    return toast;
  }
}
