import { Component } from '@angular/core';
import { FirebaseService } from "./services/firebase.serv.service";
import { Router } from "@angular/router";
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor(public auth: FirebaseService,public router: Router) {}
  checkUser(){
    if (this.auth.isLoggedIn) {
      this.router.navigate(['/home']);
    }
  }
  ngOnInit() {
    this.checkUser()
 
  }
}
