import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FirebaseService } from "./../services/firebase.serv.service";
import { Router } from "@angular/router";
import { IndicatorsService } from "../services/indicators.service";


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  public formLogin: FormGroup

  constructor(private formBuilder: FormBuilder, public auth: FirebaseService, public router: Router, public indicator: IndicatorsService
  ) { }

  ngOnInit() {
    this.formLogin = this.formBuilder.group({
      nombre: ['',
        [
          Validators.email,
          Validators.required,
          Validators.minLength(6)
        ]
      ],

      password: ['',
        [
          Validators.required,
          Validators.minLength(6)
        ]
      ]
    })
  }

  async send() {
    const loader:any = await this.indicator.generateLoader()
    await loader.present();
    const userData = { ...this.formLogin.value }
    const response = await this.auth.login(userData.nombre, userData.password);
    loader.dismiss()


  }

}
