import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePage } from './home.page';
import { TabAccountsComponent } from "./tab-accounts/tab-accounts.component";
import { TabTransferComponent } from "./tab-transfer/tab-transfer.component";
import { TabIncomingComponent } from "./tab-incoming/tab-incoming.component";
import { DetailAccountComponent } from "./detail-account/detail-account.component";
import { AddAccountsComponent } from "./add-accounts/add-accounts.component";
import { QrReadComponent } from "./qr-read/qr-read.component";
import { QrGeneratorComponent } from "./qr-generator/qr-generator.component";
import { TransfPropiasComponent } from "./transf-propias/transf-propias.component";


const routes: Routes = [
  {
    path: '',
    component: HomePage,
    children: [
      {
        path: 'accounts',
        children: [
          {
            path: '',
            component: TabAccountsComponent
          }
        ]
      },
      {
        path: 'transferecias',
        children: [
          {
            path: '',
            component: TabTransferComponent
          }
        ]
      },
      {
        path: 'recibir',
        children: [
          {
            path: '',
            component: TabIncomingComponent
          }
        ]
      }
    ]
  },
  {
    path:"detail/:tipo/:id",
    component:DetailAccountComponent
  }
  ,
  {
    path:"cuentas-terceros",
    component:AddAccountsComponent
  },
  {
    path:"transf-propias",
    component:TransfPropiasComponent
  },
  
  {
    path:"qr-read",
    component:QrReadComponent
  },
  {
    path:"qr-generator",
    component:QrGeneratorComponent
  },
  {
    path: 'home',
    redirectTo: 'home/accounts',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomePageRoutingModule {}
