import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-accounts',
  templateUrl: './add-accounts.component.html',
  styleUrls: ['./add-accounts.component.scss'],
})
export class AddAccountsComponent implements OnInit {
  public formLogin: FormGroup

  constructor(private formBuilder: FormBuilder,) { }

  ngOnInit() {
    this.formLogin = this.formBuilder.group({
      alias: ['',
        [
          Validators.required,
        ]
      ],

      entidad: ['',
        [
          Validators.required,
        ]
      ],
      tipo: ['',
        [
          Validators.required,
        ]
      ],
      numero: [0,
        [
          Validators.required,
          Validators.minLength(11)
        ]
      ],
      identificacion: [0,
        [
          Validators.required,
          Validators.minLength(12)
        ]
      ],
      moneda: ['',
        [
          Validators.required,
        ]
      ]

    })
  }
  async send() {
    // const loader:any = await this.indicator.generateLoader()
    // await loader.present();

    // loader.dismiss()


  }

  getBackButtonText() {
    const win = window as any;
    const mode = win && win.Ionic && win.Ionic.mode;
    return mode === 'ios' ? 'Home' : '';
  }

}
