import { Component, OnInit } from '@angular/core';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner/ngx';

@Component({
  selector: 'app-qr-read',
  templateUrl: './qr-read.component.html',
  styleUrls: ['./qr-read.component.scss'],
})
export class QrReadComponent implements OnInit {

  scannedData: {
    cuenta: string;
    moneda: string;
    valor: string;
    ref:string
  } = { cuenta: "", moneda: "COP", valor: "",ref:"" }
  encodedData: '';
  encodeData: any;
  inputData: any;
  constructor(private barcodeScanner: BarcodeScanner) { }
  scanBarcode() {
    const options: BarcodeScannerOptions = {
      preferFrontCamera: false,
      showFlipCameraButton: true,
      showTorchButton: true,
      torchOn: false,
      prompt: 'Place a barcode inside the scan area',
      resultDisplayDuration: 500,
      formats: 'EAN_13,EAN_8,QR_CODE,PDF_417 ',
      orientation: 'portrait',
    };

    this.barcodeScanner.scan(options).then(barcodeData => {
      console.log('Barcode data', barcodeData);
      if (barcodeData.cancelled === false && barcodeData.format === "QR_CODE") { }
      this.scannedData = JSON.parse(barcodeData.text);

    }).catch(err => {
      console.log('Error', err);
    });
  }


  getBackButtonText() {
    const win = window as any;
    const mode = win && win.Ionic && win.Ionic.mode;
    return mode === 'ios' ? 'Home' : '';
  }
  ngOnInit() { }

}
