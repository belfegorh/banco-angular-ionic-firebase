import { Component, OnInit } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { AccountsService } from "../../services/accounts.service";

@Component({
  selector: 'app-qr-generator',
  templateUrl: './qr-generator.component.html',
  styleUrls: ['./qr-generator.component.scss'],
})
export class QrGeneratorComponent implements OnInit {

  cuentas: {
    "corrientes": {},
    "ahorros": {}
  } = {
      "corrientes": {},
      "ahorros": {}
    }
  ahorros:any={}
  corrientes:any={}
  data: {
    cuenta: string;
    moneda: string;
    valor: string;
    ref:string
  } = { cuenta: "", moneda: "COP", valor: "",ref:"" }
  qrData:any;
  constructor(
    private barcodeScanner: BarcodeScanner,
    private accountsService: AccountsService
  ) {
    this.getAccounts()
  }


  createBarcode() {
    this.barcodeScanner.encode(this.barcodeScanner.Encode.TEXT_TYPE, JSON.stringify(this.data)).then((encodedData) => {
      this.qrData = encodedData;
    }, (err) => {
      console.log('Error occured : ' + err);
    });
  }
  getBackButtonText() {
    const win = window as any;
    const mode = win && win.Ionic && win.Ionic.mode;
    return mode === 'ios' ? 'Home' : '';
  }
  ngOnInit() {

  }
  async getAccounts() {
    const response: any = await this.accountsService.getAccounts()
    this.ahorros = response.data().ahorros
    this.corrientes = response.data().corrientes

  }
}
