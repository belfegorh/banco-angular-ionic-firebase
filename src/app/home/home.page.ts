import { Component } from '@angular/core';
import { FirebaseService } from "../services/firebase.serv.service";

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(public auth:FirebaseService) {}
}
