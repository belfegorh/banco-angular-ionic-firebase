import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { HomePage } from './home.page';

import { HomePageRoutingModule } from './home-routing.module';
import { TabAccountsComponent } from "./tab-accounts/tab-accounts.component";
import { TabTransferComponent } from "./tab-transfer/tab-transfer.component";
import { TabIncomingComponent } from "./tab-incoming/tab-incoming.component";
import { DetailAccountComponent } from "./detail-account/detail-account.component";
import { AddAccountsComponent } from "./add-accounts/add-accounts.component";
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { QrReadComponent } from "./qr-read/qr-read.component";
import { QrGeneratorComponent } from "./qr-generator/qr-generator.component";
import { TransfPropiasComponent } from "./transf-propias/transf-propias.component";

import { AccountsService } from "../services/accounts.service";


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    HomePageRoutingModule
  ],
  providers:[BarcodeScanner,AccountsService],
  declarations: [HomePage, 
    TabAccountsComponent, 
    TabTransferComponent, 
    TabIncomingComponent, 
    DetailAccountComponent, 
    AddAccountsComponent,
    QrReadComponent,
    QrGeneratorComponent,
    TransfPropiasComponent
  ]
})
export class HomePageModule { }
