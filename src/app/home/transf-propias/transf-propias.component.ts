import { Component, OnInit } from '@angular/core';
import { AccountsService } from "../../services/accounts.service";
import { IndicatorsService } from "../../services/indicators.service";

@Component({
  selector: 'app-transf-propias',
  templateUrl: './transf-propias.component.html',
  styleUrls: ['./transf-propias.component.scss'],
})
export class TransfPropiasComponent implements OnInit {
  ahorros: any = {}
  corrientes: any = {}
  data: {
    cuenta_origen: string;
    cuenta_destino: string;
    descripcion: string;
    moneda: string;
    monto: number;
    num_transf: string
  } = { cuenta_origen: "", cuenta_destino: "", moneda: "COP", monto: 0, num_transf: "", descripcion: "" }
  constructor(
    private accountsService: AccountsService,
    public indicator: IndicatorsService,

  ) {
    this.getAccounts()

  }

  ngOnInit() { }

  async getAccounts() {
    const response: any = await this.accountsService.getAccounts()
    this.ahorros = response.data().ahorros
    this.corrientes = response.data().corrientes
    console.log(this.ahorros)

  }
  async updateDoc(doc) {
    const loader: any = await this.indicator.generateLoader()
    await loader.present();

    const res = await this.accountsService.updateAccounts(doc).then(async res => {
      const toast: any = await this.indicator.presentToast("success", "Alias actualizado correctamente")
      toast.present();
    }).catch(async err => {
      const toast: any = await this.indicator.presentToast("danger", "Error actualizando el alias")
      toast.present();
    })
    loader.dismiss()
  }
  async send() {
    if ((this.validateCuentas() && this.validateSaldo())) {
      console.log(this.buildDocresta())
      const loader: any = await this.indicator.generateLoader()
      await loader.present();

      const origen = await this.accountsService.updateAccounts(this.buildDocresta()).then(async res => {
        const toast: any = await this.indicator.presentToast("success", "Cuenta actualizada")
        toast.present();
      }).catch(async err => {
        const toast: any = await this.indicator.presentToast("danger", "Error actualizando el cuenta")
        toast.present();
      })
      const destino = await this.accountsService.updateAccounts(this.buildDocsuma()).then(async res => {
        const toast: any = await this.indicator.presentToast("success", "Cuenta actualizada")
        toast.present();
      }).catch(async err => {
        const toast: any = await this.indicator.presentToast("danger", "Error actualizando el cuenta")
        toast.present();
      })
      loader.dismiss()
    } else {

    }
  }

  buildDocresta() {
    if (this.ahorros.hasOwnProperty(this.data.cuenta_origen)) {
      let doc = {}
      const nuevoSAldo = (this.ahorros[this.data.cuenta_origen].saldo - this.data.monto)
      doc["ahorros"] = {}
      doc["ahorros"][this.data.cuenta_origen] = { saldo: nuevoSAldo }
      return doc
    }
    if (this.corrientes.hasOwnProperty(this.data.cuenta_origen)) {
      let doc = {}
      const nuevoSAldo = (this.corrientes[this.data.cuenta_origen].saldo - this.data.monto)
      doc["corrientes"] = {}
      doc["corrientes"][this.data.cuenta_origen] = { saldo: nuevoSAldo }
      return doc
    }
  }
  buildDocsuma() {
    if (this.ahorros.hasOwnProperty(this.data.cuenta_destino)) {
      let doc = {}
      const nuevoSAldo = (this.ahorros[this.data.cuenta_destino].saldo + this.data.monto)
      doc["ahorros"] = {}
      doc["ahorros"][this.data.cuenta_destino] = { saldo: nuevoSAldo }
      return doc
    }
    if (this.corrientes.hasOwnProperty(this.data.cuenta_destino)) {
      let doc = {}
      const nuevoSAldo = (this.corrientes[this.data.cuenta_destino].saldo + this.data.monto)
      doc["corrientes"] = {}
      doc["corrientes"][this.data.cuenta_destino] = { saldo: nuevoSAldo }
      return doc
    }
  }
  async validateSaldo() {

    if (this.ahorros.hasOwnProperty(this.data.cuenta_origen)) {
      const saldo = this.ahorros[this.data.cuenta_origen].saldo < this.data.monto ? false : true;
      if (!saldo) {
        const toast: any = await this.indicator.presentToast("warning", "Saldo insuficiente")
        toast.present();
      }
      return saldo
    }
    if (this.corrientes.hasOwnProperty(this.data.cuenta_origen)) {
      const saldo = this.corrientes[this.data.cuenta_origen].saldo < this.data.monto ? false : true;
      if (!saldo) {
        const toast: any = await this.indicator.presentToast("warning", "Saldo insuficiente")
        toast.present();
      }
      return saldo
    }
    return false

  }
  cuentOrigen() {

  }
  async validateCuentas() {
    if (this.data.cuenta_destino !== this.data.cuenta_origen) {
      return true
    } else {
      const toast: any = await this.indicator.presentToast("warning", "Las cuentas de origen y destino deben ser diferentes")
      toast.present();
      return false
    }

  }
  getBackButtonText() {
    const win = window as any;
    const mode = win && win.Ionic && win.Ionic.mode;
    return mode === 'ios' ? 'Home' : '';
  }

}
