import { Component, OnInit } from '@angular/core';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner/ngx';

@Component({
  selector: 'app-tab-incoming',
  templateUrl: './tab-incoming.component.html',
  styleUrls: ['./tab-incoming.component.scss'],
})
export class TabIncomingComponent implements OnInit {

  scannedData: {
    cuenta: string;
    banco: string;
    monto: string
  } = {cuenta:"",banco:"",monto:""}
  encodedData: '';
  encodeData: any;
  inputData: any;
  constructor(private barcodeScanner: BarcodeScanner) { }
  scanBarcode() {
    const options: BarcodeScannerOptions = {
      preferFrontCamera: false,
      showFlipCameraButton: true,
      showTorchButton: true,
      torchOn: false,
      prompt: 'Place a barcode inside the scan area',
      resultDisplayDuration: 500,
      formats: 'EAN_13,EAN_8,QR_CODE,PDF_417 ',
      orientation: 'portrait',
    };

    this.barcodeScanner.scan(options).then(barcodeData => {
      console.log('Barcode data', barcodeData);
      if (barcodeData.cancelled === false && barcodeData.format === "QR_CODE") { }
      this.scannedData = JSON.parse(barcodeData.text);

    }).catch(err => {
      console.log('Error', err);
    });
  }

  createBarcode() {
    this.barcodeScanner.encode(this.barcodeScanner.Encode.TEXT_TYPE, this.inputData).then((encodedData) => {
      console.log(encodedData);
      this.encodedData = encodedData;
    }, (err) => {
      console.log('Error occured : ' + err);
    });
  }

  ngOnInit() { }

}
