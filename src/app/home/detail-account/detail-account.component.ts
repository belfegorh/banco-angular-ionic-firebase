import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IndicatorsService } from "../../services/indicators.service";
import { AccountsService } from "../../services/accounts.service";


@Component({
  selector: 'app-detail-account',
  templateUrl: './detail-account.component.html',
  styleUrls: ['./detail-account.component.scss'],
})
export class DetailAccountComponent implements OnInit {
  id = this.activatedRoute.snapshot.paramMap.get('id');
  tipo = this.activatedRoute.snapshot.paramMap.get('tipo');
  

  public account = {}

  constructor(
    private activatedRoute: ActivatedRoute,
    private accountsService: AccountsService,
    public indicator:IndicatorsService
  ) {

    this.getAccounts()

  }

  ngOnInit() {
   
  }
  ngOnDestroy() {
  }
  getBackButtonText() {
    const win = window as any;
    const mode = win && win.Ionic && win.Ionic.mode;
    return mode === 'ios' ? 'Home' : '';
  }
  async getAccounts() {
    const response: any = await this.accountsService.getAccounts()
    const res = response.data()
    this.account = { ...res[this.tipo][this.id] };

  }
  async updateDoc(alias: string) {
    const loader:any = await this.indicator.generateLoader()
    await loader.present();
    let doc = {}
    doc[this.tipo] = {}
    doc[this.tipo][this.id] = { alias: alias }
    const res = await this.accountsService.updateAccounts(doc).then(async res=>{
      const toast:any = await this.indicator.presentToast("success","Alias actualizado correctamente")
      toast.present();
    }).catch(async err=>{
      const toast:any = await this.indicator.presentToast("danger","Error actualizando el alias")
      toast.present();
    })
    loader.dismiss()
  }


}
