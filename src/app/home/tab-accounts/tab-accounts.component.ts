import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';

import { AccountsService } from "../../services/accounts.service";

@Component({
  selector: 'app-tab-accounts',
  templateUrl: './tab-accounts.component.html',
  styleUrls: ['./tab-accounts.component.scss'],
})
export class TabAccountsComponent implements OnInit {


  public accounts: {
    "corrientes": {},
    "ahorros": {}
  } = {
      "corrientes": {},
      "ahorros": {}
    }
  public displayAccounts = "corrientes"

  constructor(firestore: AngularFirestore,     private accountsService: AccountsService
    ) {
  
  }

  ngOnInit() {
    this.getAccounts()

   
  }
  async getAccounts() {
    const response: any = await this.accountsService.getAccounts()
    this.accounts = response.data()

  }
  segmentChanged(ev: any) {
    console.log('Segment changed', ev.detail.value);
    this.displayAccounts = ev.detail.value
  }
  keys(obj:Object):string[]{
    return Object.keys(obj)
  }

}


